//
//  ViewController.m
//  TestImageCrop
//
//  Created by guozhicheng on 12/7/15.
//  Copyright © 2015 guozhicheng. All rights reserved.
//

#import "ViewController.h"
#import "FCFileManager.h"
#import "SDImageCache.h"
#import "UIImage+Resizing.h"
#import "KSTestCrop.h"

@interface ViewController ()

@property (nonatomic) UIImageView *upImageView;
@property (nonatomic) UIImageView *bottomImageView;

@property (nonatomic) UIImageView *imageView;

@property (nonatomic) UIImage *resultImage;

@property (nonatomic) UIImage *cropedImage;

@property (nonatomic) SDImageCache *sdCache;

@end

@implementation ViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [super viewDidLoad];
    
    self.sdCache = [[SDImageCache alloc] initWithNamespace:@"testCache"];
    self.resultImage = [UIImage imageNamed:@"sourceImage.jpg"];
    
    NSLog(@"filePath %@", [FCFileManager pathForDocumentsDirectory]);
    
//    self.imageView = [[UIImageView alloc] init];
//    self.imageView.frame = self.view.bounds;
//    
//    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
//    
//    
//    self.imageView.image = self.resultImage;
//    
//    [self.sdCache storeImage:self.resultImage forKey:@"originImage"];
//    
//    [self.view addSubview:self.imageView];
//    
//    NSLog(@"imageSize w %f h %f",self.resultImage.size.width,self.resultImage.size.height);
//    
//    CGFloat width = self.resultImage.size.width;
//    CGFloat height = width * 9 / 16;
//    
//    CGRect resultRect = CGRectMake(0, self.resultImage.size.height - height, width, height);
//    
//    self.cropedImage = [self.resultImage cropImage:resultRect];
//    
//    [self.sdCache storeImage:self.cropedImage forKey:@"cropedImage"];
//    
//    UIImage *errorTestImage = [KSTestCrop testCropImage];
//    
//    [self.sdCache storeImage:errorTestImage forKey:@"testcropedImage"];
//    
//    self.imageView.image = self.cropedImage;
    
    /**
     *  这个图在iPhone 5 上面出来的是错的图
     */
    [self cropRightImage];
    
    /**
     *  这个图是正确的, 看请文件
     */
    [self cropErrorImage];
    
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.upImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2);
    self.bottomImageView.frame = CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, self.view.frame.size.height/2);
}

- (UIImage *) cropRightImage {
    CGFloat width = self.resultImage.size.width;
    CGFloat height = width * 9 / 16;
    
    CGRect resultRect = CGRectMake(0, self.resultImage.size.height - height, width, height);
     self.cropedImage = [self.resultImage cropImage:resultRect];
    
     [self.sdCache storeImage:self.cropedImage forKey:@"cropRightImage"];
    
    return self.cropedImage;
}

- (UIImage *) cropErrorImage {
    UIImage *testImage = [UIImage imageNamed:@"sourceImage.jpg"];
    
    NSLog(@"testImage width %f height %f",testImage.size.width,testImage.size.height);
    
    SDImageCache *imageCache = [[SDImageCache alloc] initWithNamespace:@"testNameSpace"];
    
    CGFloat width = testImage.size.width;
    CGFloat height = width * 9 / 16;
    
    CGRect resultRect = CGRectMake(0, testImage.size.height - height, width, height);
    
    UIImage *resultImage = [testImage cropImage:resultRect];
    
    [imageCache storeImage:resultImage forKey:@"cropErrorImage"];
    
    return resultImage;
}


@end
