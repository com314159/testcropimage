//
//  KSTestCrop.h
//  KeyShare
//
//  Created by guozhicheng on 12/8/15.
//  Copyright © 2015 Jaxus. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface KSTestCrop : NSObject

+ (UIImage *) testCropImage;

+ (UIImage *) testConvert:(UIImage *)preImage;

@end
