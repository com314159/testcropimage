//
//  KSTestCrop.m
//  KeyShare
//
//  Created by guozhicheng on 12/8/15.
//  Copyright © 2015 Jaxus. All rights reserved.
//

#import "KSTestCrop.h"
#import "UIImage+Resizing.h"
#import "SDImageCache.h"

@implementation KSTestCrop

+ (UIImage *) testCropImage {
    UIImage *testImage = [UIImage imageNamed:@"sourceImage.jpg"];
    
    NSLog(@"testImage width %f height %f",testImage.size.width,testImage.size.height);
    
    SDImageCache *imageCache = [[SDImageCache alloc] initWithNamespace:@"testNameSpace"];
    
    CGFloat width = testImage.size.width;
    CGFloat height = width * 9 / 16;
    
    CGRect resultRect = CGRectMake(0, testImage.size.height - height, width, height);
    
    UIImage *resultImage = [testImage cropImage:resultRect];
    
    [imageCache storeImage:resultImage forKey:@"testKey"];
    
    return resultImage;
}

+ (UIImage *) testConvert:(UIImage *)preImage {
    NSData *data = UIImageJPEGRepresentation(preImage, 1);
    
    UIImage *resultImage = [[UIImage alloc] initWithData:data];
    return resultImage;
}

@end
